<?php

/* 
 * Define your API's and comments here. Sample given below. The documentation page will automatically read the API comments 
 * and create documentation out of it.
 * 
 * License: Open source - MIT
 * Please visit http://opensource.org/licenses/MIT for more information
 * 
 * Credits: 
 * Vertical reponsive menu 
 * (https://github.com/cbfranca/vertical-responsive-menu)
 */

/*Includes*/
/*All include files go here*/
session_start();



/**
 * <h1>Sample_API</h1>
 * <p>Give a description of the API here.</p>
 * <b>Parameters</b><br>
 * <table class="tbl_param">
 * <tr><th class="col_key">Key</th><th class="col_datatype">Data Type</th><th class="col_desc">Description</th><th class="col_defval">Default Value</th><th class="col_unit">Unit</th></tr>
 * <tr><td>Parameter 1</td><td>Data Type</td><td>Value 1</td><td></td><td></td></tr>
 * <tr><td>Parameter 2</td><td>Data Type</td><td>Value 2</td><td></td><td></td></tr>
 * <tr><td>Parameter 3</td><td>Data Type</td><td>Value 3</td><td></td><td>Kmph</td></tr>
 * <tr><td>Parameter 4</td><td>Data Type</td><td>Value</td><td></td><td>Km</td></tr>
 * </table>
 * <b>Returns</b><br>
 * <table class="tbl_ret">
 * <tr><th class="col_key">Key</th><th class="col_datatype">Data Type</th><th class="col_ret_desc">Description</th><th class="col_unit">Unit / Format</th></tr>
 * <tr><td>Return Value 1</td><td>Data Type</td><td>Description</td><td>hr</td></tr>
 * </table>
 * <b>URL</b><br>
 * <p>
 * <input size="130" type="text" value="www.try.com" id="get_elevation_copy" class="disabled" readonly="readonly">
 * </p>
 * @param $config
 * @param $p
 */
function Sample_API($params) {
    echo "Sample API";
}

/**
 * <h1>Sample_API_2</h1>
 * <p>Give a description of the API here.</p>
 * <b>Parameters</b><br>
 * <table class="tbl_param">
 * <tr><th class="col_key">Key</th><th class="col_datatype">Data Type</th><th class="col_desc">Description</th><th class="col_defval">Default Value</th><th class="col_unit">Unit</th></tr>
 * <tr><td>Parameter 1</td><td>Data Type</td><td>Value 1</td><td></td><td></td></tr>
 * <tr><td>Parameter 2</td><td>Data Type</td><td>Value 2</td><td></td><td></td></tr>
 * <tr><td>Parameter 3</td><td>Data Type</td><td>Value 3</td><td></td><td>Kmph</td></tr>
 * <tr><td>Parameter 4</td><td>Data Type</td><td>Value</td><td></td><td>Km</td></tr>
 * </table>
 * <b>Returns</b><br>
 * <table class="tbl_ret">
 * <tr><th class="col_key">Key</th><th class="col_datatype">Data Type</th><th class="col_ret_desc">Description</th><th class="col_unit">Unit / Format</th></tr>
 * <tr><td>Return Value 1</td><td>Data Type</td><td>Description</td><td>hr</td></tr>
 * </table>
 * <b>URL</b><br>
 * <p>
 * <input size="130" type="text" value="www.try.com" id="get_elevation_copy" class="disabled" readonly="readonly">
 * </p>
 * @param $config
 * @param $p
 */
function Sample_API($params) {
    echo "Sample API";
}
    
