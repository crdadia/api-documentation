<?php

/* 
 * Define your API's and comments here. Sample given below. The documentation page will automatically read the API comments 
 * and create documentation out of it.
 * 
 * License: Open source - MIT
 * Please visit http://opensource.org/licenses/MIT for more information
 * 
 * Credits: 
 * Vertical reponsive menu 
 * (https://github.com/cbfranca/vertical-responsive-menu)
 */



$api_locs= ['index.php'];

$insideFunc = false;
$content = [];

foreach ($api_locs as $api_loc) {
    $handler = fopen($api_loc, 'r');
    while (($buffer = fgets($handler, 4096)) !== false) {
        // skip PHPdoc data
        if (substr($buffer, 0, 4) == ' * @') {
            continue;
        }

        $buffer = trim($buffer);

        if (strpos($buffer, '<h1>')) {
            $insideFunc = true;
            $func = substr($buffer, 6, -5);
            continue;
        }
        if (substr($buffer, 0, 2) == '*/') {
            $insideFunc = false;
        }

        if (!$insideFunc) {
            continue;
        }

        if (empty($content[$func])) {
            $content[$func] = '';
        }

        $content[$func] .= substr($buffer, 2);
    }
    fclose($handler);
}

ksort($content);
?>﻿

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>API Documentation Page</title>
        <link rel="shortcut icon" href="utils/img/favicon.png" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet'>
        <link href='utils/css/normalize.css' rel='stylesheet'>
        <link href='utils/fontawesome/css/font-awesome.min.css' rel='stylesheet'>
        <link href="utils/css/vertical-responsive-menu.css" rel="stylesheet">
        <link href="utils/css/documentation.css" rel="stylesheet">
    </head>
    <style>
        a {
            color: white;
            text-decoration: none;
          }

        a:hover {
            cursor: pointer;
            text-decoration: underline;
          }

        a:active {
            color: black;
          }

        a:visited {
            color: white;
          }

    </style>


    <body>

        <header class="header clearfix">
            <button type="button" id="toggleMenu" class="toggle_menu">
                <i class="fa fa-bars"></i>
            </button> 
            <img src="utils/img/Logo.png" alt="LOGO" width="200"/>
        </header>

        <nav class="vertical_nav">
            <input type="text" id="searchBar" onkeyup="search()" placeholder="&#xF002; &nbsp;&nbsp;&nbsp; Search API..." style="font-family:Arial, FontAwesome" >
            
            <ul id="js-menu" class="menu" style="margin-top: 40px;">
                <!--Code for Types of Menu and Sub-Menu Items-->
                <!--      <li class="menu--item  menu--item__has_sub_menu">
                
                        <label class="menu--link" title="Item 1">
                          <i class="menu--icon  fa fa-fw fa-user"></i>
                          <span class="menu--label">Item 1</span>
                        </label>
                
                        <ul class="sub_menu">
                          <li class="sub_menu--item">
                            <a href="http://google.com" target="_blank" class="sub_menu--link sub_menu--link__active">Submenu</a>
                          </li>
                          <li class="sub_menu--item">
                            <a href="#" class="sub_menu--link">Submenu</a>
                          </li>
                          <li class="sub_menu--item">
                            <a href="#" class="sub_menu--link">Submenu</a>
                          </li>
                        </ul>
                      </li>
                
                      <li class="menu--item">
                        <a href="#" class="menu--link" title="Item 2">
                          <i class="menu--icon  fa fa-fw fa-briefcase"></i>
                          <span class="menu--label">Item 2</span>
                        </a>
                      </li>-->

                <?php
                foreach ($content as $func => $desc) {
                    echo "<li class='menu--item'>";
                    echo '<a href="#' . $func . '" class="menu--link">';
                    echo "<i class='menu--icon  fa fa-fw fa-circle-o'></i>";
                    echo "<span class='menu--label'>" . $func . "</span>";
                    echo "</a>";
                    echo "</li>";
                }
                ?>
            </ul>

            <!--Code for Sidebar Toggle-->

            <!--            <button id="collapse_menu" class="collapse_menu">
                            <i class="collapse_menu--icon  fa fa-fw"></i>
                            <span class="collapse_menu--label">Recolher menu</span>
                        </button>-->
        </nav>


        <div class="wrapper">
            <div id='Intro'>
                <h1 style='text-align: center;'>API Documentation</h1>
                <hr>
                <p>
                    The documentation page auto-reads the API documentation from the API index page and displays it here. The API index page may consist of multiple API's. The developer
                    is required to provide API description (commented) in the format given on the index page. The documentation page will automatically read and display the API description.
                    The documentation page also allows the users to search documentation of a specific API using the Search Tab in the Left Hand Side Column. 
                </p>
                <hr>
                <h2 style='text-align: center;'>Credits</h2>
                <ul>
                    <li><a href='https://github.com/cbfranca/vertical-responsive-menu' target='_blank'> Vertical Responsive Menu</a></li>
                </ul>
            </div>

            <div id="api_content">
                <?php
                foreach ($content as $func => $desc) {
                    echo '<hr id="'.$func.'"><br><br>';
                    echo '<h1>' . $func . "</h1>\n";
                    echo $desc . "\n";
                }
                ?>
            </div>
        </div>

        <script src="utils/js/vertical-responsive-menu.js"></script>
        <script>
            
            function search() {
                var input, filter, ul, li, a, i;
                input = document.getElementById("searchBar");
                filter = input.value.toUpperCase();
                ul = document.getElementById("js-menu");
                li = ul.getElementsByTagName("li");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }
            

        </script>
    </body>
</html>